var express = require('express');
var router = express.Router();
var Offre = require('../models/offre');
var mongo = require('mongodb');


router.get('/', function(req, res, next){
  Offre.find({}, function(err, currentoffres){
    if (err) throw err;
    res.render('offres', { title: 'Offres', offres: currentoffres});
  });
});

router.post('/updateOffers', function(req, res, next){
  var getId = req.body.id;
  var objectId = new mongo.ObjectID(getId);
  Offre.find({
    _id: objectId
  }, function(err, currentoffres){
    if (err) throw err;
    res.render('updateOffers', { title: 'updateOffers', updateOffers: currentoffres});
  });
});

router.post('/modificationOffers', function(req, res, next){
  var getId = req.body.id;
  var objectId = new mongo.ObjectID(getId);
  var nameEvent = req.body.nameEvent;
  var typeEvent = req.body.typeEvent;
  var dateBegin = req.body.dateBegin;
  var nbDays = req.body.nbDays;
  var demandRole = req.body.demandRole;
  Offre.update(
    {"_id": objectId},
     { $set: { 
       "nameEvent" : nameEvent,
       "typeEvent" : typeEvent,
       "dateBegin" : dateBegin,
       "nbDays" : nbDays,
       "demandRole" : demandRole
      }
    }, function (err, doc) {
       if (err) {
         res.send("Pas glop !");
       } else {
          // Redirection vers la liste, donc vers une vue existante
          res.redirect("/offres");
       }
    });
 });

router.get('/addOffers', function (req, res, next){
  res.render('addOffers');
});
/* Ajout d'un utilisateur */
router.post('/addOffer', function(req, res, next) {
  var db = req.db;
  var nameEvent = req.body.nameEvent;
  var typeEvent = req.body.typeEvent;
  var dateBegin = req.body.dateBegin;
  var nbDays = req.body.nbDays;
  var demandRole = req.body.demandRole;
  var collection = db.get('offres');
  collection.insert({
    "nameEvent" : nameEvent,
    "typeEvent" : typeEvent,
    "dateBegin": dateBegin,
    "nbDays" : nbDays,
    "demandRole" : demandRole
  }, function (err, doc) {
      if (err) {
        res.send("Pas glop !");
      } else {
         // Redirection vers la liste, donc vers une vue existante
         res.redirect("/offres");
      }
   });
});

router.post('/delOffers', function(req, res, next) {
  var db = req.db;
  var getId = req.body.id;
  var collection = db.get('offres');
  var objectId = new mongo.ObjectID(getId);
  collection.remove({
    _id: objectId
  }, function(err, doc){
    if(err) {
      res.send("Pas glop !");
    } else {
      res.redirect("/offres");
    }
  });
});


module.exports = router;
