var Candidature = require('../models/candidature');
var Figurant = require('../models/figurant');
var Offre = require('../models/offre');
var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var path = require('path');
var fs = require('fs');

router.get('/', function (req, res, next) {

    Candidature.aggregate([
        {
            $lookup: {
                from: "offres",
                localField: "offre",
                foreignField: "_id",
                as: "offre"
            }
        },
        {
            $lookup: {
                from: "figurants",
                localField: "figurant",
                foreignField: "_id",
                as: "figurant"
            }
        }
    ]).exec(function (err, currentcandidature) {
        if (err) throw err;
        res.render('candidatures', { title: 'Candidatures', candidatures: currentcandidature });
    });
});


// Update candidacy
router.post('/update/:id', function (req, res, next) {
    Candidature.findByIdAndUpdate(req.params.id, {
      etat: req.body.etat
    }, function (err, doc) {
      if (err) {
        console.log('error');
      }
      else {
        res.redirect('/candidatures');
        console.log(doc);
      }
    });
  });

module.exports = router;