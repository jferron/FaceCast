var express = require('express');
var router = express.Router();
var Figurant = require('../models/figurant');
var mongo = require('mongodb');

router.get('/', function (req, res, next) {
  Figurant.find({}, function (err, currentfigurants) {
    if (err) throw err;
    res.render('figurants', { title: 'Figurants', figurants: currentfigurants });
  });
});

router.post('/updateFigurant', function (req, res, next) {
  var getId = req.body.id;
  var objectId = new mongo.ObjectID(getId);
  Figurant.find({
    _id: objectId
  }, function (err, currentfigurant) {
    if (err) throw err;
    res.render('updateFigurants', { title: 'updateFigurants', updateFigurants: currentfigurant });
  });
});

router.post('/modificationFigurant', function (req, res, next) {
  var getId = req.body.id;
  var objectId = new mongo.ObjectID(getId);
  var surName = req.body.surName;
  var firstName = req.body.firstName;
  var email = req.body.email;
  Figurant.update(
    { "_id": objectId },
    {
      $set: {
        "surName": surName,
        "firstName" : firstName,
        "email" : email
      }
    }
    , function (err, doc) {
      if (err) {
        res.send("Pas glop !");
      } else {
        // Redirection vers la liste, donc vers une vue existante
        res.redirect("/figurants");
      }
    });
});

/* Formulaire insertion d'un figurant */
router.get('/insertFigurant', function (req, res, next) {
  res.render('insertFigurant');
});

/* Ajout d'un figurant */
router.post('/addFigurant', function (req, res, next) {
  var db = req.db;
  var figurantNom = req.body.nom;
  var figurantPrenom = req.body.prenom;
  var figurantEmail = req.body.email;
  var collection = db.get('figurants');
  collection.insert({
    "surName": figurantNom,
    "firstName": figurantPrenom,
    "email": figurantEmail
  }, function (err, doc) {
    if (err) {
      res.send("Pas glop !");
    } else {
      // Redirection vers la liste, donc vers une vue existante
      res.redirect("/figurants");
    }
  });
});

/* Suppression d'un figurant */
router.post('/remFigurant', function (req, res, next) {
  var getId = req.body.id;
  var objectId = new mongo.ObjectID(getId);
  Figurant.remove({
    _id: objectId
  }, function (err, doc) {
    if (err) {
      res.send("Pas glop !");
    } else {
      res.redirect("/figurants");
    }
  });
});

module.exports = router;