var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stylus = require('stylus');

//Base de données
var mongo = require('mongodb');
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/castjs';
mongoose.connect(mongoDB, {

  useMongoClient: true

});


var dbm = mongoose.connection;


dbm.on('error', console.error.bind(console, 'MongoDB connection error:'));
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var monk = require('monk');
var db = monk('localhost:27017/castjs');

// Déclaration API Rest
var verification = require('./rest/verification');
var restOffres = require('./rest/restOffres');
var restFigurants = require('./rest/restFigurants');
var restCandidatures = require('./rest/restCandidatures');

// Déclaration des vues
var index = require('./routes/index');
var figurants = require('./routes/figurants');
var offres = require('./routes/offres');
var candidatures = require('./routes/candidatures');

var app = express();
app.locals.pretty = true;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
 }));
 app.use(passport.initialize());
 app.use(passport.session());


app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req,res,next){
  req.db = db;
  next();
 });

app.use('/', index);
app.use('/figurants', figurants);
app.use('/offres', offres);
app.use('/candidatures', candidatures);

app.use('/rest/verification', verification);
app.use('/rest/restOffres', restOffres);
app.use('/rest/restFigurants', restFigurants);
app.use('/rest/restCandidatures', restCandidatures);

// Configuration de Passport
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
