var express = require('express');
var router = express.Router();
var Figurants = require('../models/figurant.js');



/*router.get('/', function(req, res, next) {
  Figurants.find({}, function(err, currentfigurants){
    if (err){
      throw err;
    }
    liste = res.json(currentfigurants);

  });
});*/

router.get('/:email', function (req, res, next) {
  Figurants.find(
    {email: req.params.email}, 
    function (err, currentfigurant) {
    if (err) throw err;
    liste = res.json(currentfigurant);
  });
});

module.exports = router;
