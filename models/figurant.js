var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var figurantSchema = Schema({
    _id:Schema.Types.ObjectId,
    firstName:String,
    surName:String,
    email:String,
    figurant: [{ type: Schema.Types.ObjectId, ref: 'Figurant'}]
},{collection: 'figurants'});

var Figurant = mongoose.model('Figurant', figurantSchema);

module.exports = Figurant;