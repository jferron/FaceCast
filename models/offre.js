var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var offerSchema = Schema({
    _id:Schema.Types.ObjectId,
    nameEvent:String,
    typeEvent:String,
    dateBegin:String,
    nbDays:0,
    demandRole:String,
    key: [{ type: Schema.Types.String, ref: 'key'}]
},{collection: 'offres'});


var Offre = mongoose.model('Offre', offerSchema);

module.exports = Offre;