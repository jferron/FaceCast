var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var candidatureSchema = Schema({

    etat: { type: String, default: 'Waiting' },
    idExtra: { type: Schema.Types.ObjectId, ref: 'extras' },
    idOffer: { type: Schema.Types.ObjectId, ref: 'offers' }
}, { collection: 'candidatures' });

var Candidature = mongoose.model('Candidature', candidatureSchema);

module.exports = Candidature;